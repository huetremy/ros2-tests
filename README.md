# ROS2 Tests

Tests et doc divers pour la prise en main de ROS2.

Disclaimer : ce repo s'adresse aux personnes ayant déjà une connaissance relative de ROS1.

## Post-installation

Après avoir installé ros suivant [la documentation](https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html), je recommande l'ajout des lignes suivantes au `zshrc` :

```
# Auto-source ros2 humble
source /opt/ros/humble/setup.zsh
# Alias because typing `2` is a nightmare
alias ros="ros2"
# Auto-complete for zsh
eval "$(register-python-argcomplete3 ros2)"
eval "$(register-python-argcomplete3 colcon)"
```

Si les noeuds talker et listener (cf doc) ne parviennent pas à communiquer, cela peut être parce que le multicast n'est pas autorisé. Voir [cette documentation](https://docs.ros.org/en/humble/How-To-Guides/Installation-Troubleshooting.html#enable-multicast)

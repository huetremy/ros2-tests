# Beginner - Client Libraries

## Colcon

Build tool universel par dessus différents build systems (ament, setuptools, cmake...).
recommandé `ament_cmake` et `ament_python`, mais par exemple possibilité d'utiliser des `cmake` purs.

## Création de workspace

Un dossier qui contient des packaces dans un dossier `src` (comme ROS1).

`colcon build` **à la racine du workspace** pour construire, puis source.

## Création de packages

Les packages utilisent ament comme build system, colcon comme build tool.
Création par `ros2 pkg`

### C++ / CMake

Structure :

```
package/
    CMakeLists.txt
    package.xml
```

Création:

```
ros2 pkg create --build-type ament_cmake --node-name my_node my_package
```

(`node-name` pour la création d'un simple noeud hello world)

### Python

Structure:

```
my_package/
    setup.cfg
    setup.py
    package.xml
    resource/my_package
```

Création:

```
ros2 pkg create --build-type ament_python --node-name my_node my_package
```

Il faut modifier le package.xml mais aussi le setup.py

## Création de noeuds

### C++

Voir :

- [cpp_pubsub](../src/test_ws/src/cpp_pubsub/) (publisher / subscriber)
- [cpp_srvcli](../src/test_ws/src/cpp_srvcli/) (service server / client)

### Python

Voir :

- [py_pubsub](../src/test_ws/src/py_pubsub/)
- [py_srvcli](../src/test_ws/src/py_srvcli/)

## Interfaces

Création de simple messages & service : [tutorial_interfaces](../src/test_ws/src/tutorial_interfaces).
On a ici créé une simple interface, que l'on peut utiliser dans un autre package.

Note : la génération d'interfaces **requiert** l'utilisation d'`ament_cmake`. C'est une bonne pratique de créer des packages séparés pour la définition d'interfaces plutôt que de le faire dans un package créant des noeuds, mais on peut le faire tout de même: [more_interfaces](../src/test_ws/src/more_interfaces).

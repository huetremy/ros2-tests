# Concepts clefs - CLI

## Node

Similaire à ROS1 dans le principe, responsable d'une tache et communique avec les autres nodes.
Peut être un exécutable standalone, mais il est possible d'avoir plusieurs nodes dans un même process.

Les nodes sont lancés avec la commande `ros2 run`:

```
ros2 run <package_name> <node_name>
```

Possibilité de remap des paramètres du noeud (nom, topics...)

```
ros2 run turtlesim turtlesim_node --ros-args --remap __node:=my_turtle
```

Inspection avec `ros2 node`

## Topic

Similaire à ROS1. Commande `ros2 topic`
Pour l'ensemble des messages (interfaces) in peut utiliser la commande `ros2 interface`

![Illustration du concept de topics](./imgs/Topic-MultiplePublisherandMultipleSubscriber.gif)

## Services

Requête-réponse, plusieurs clients possibles mais 1 seul serveur.

`ros2 service`

![Illustration du concept de service](./imgs/Service-MultipleServiceClient.gif)

## Actions

Inclus de base dans ROS2.
Utilisées pour les taches avec un temps d'éxécution long.

Trois parties:

- Un goal (but) correspondant à un service
- Un feedback (topic) du serveur vers le client
- Un result (service).

Commande `ros2 action`

strucure de l'interface: 
```
goal
---
result
---
feedback
```

![Illustration du concept d'action](./imgs/Action-SingleActionClient.gif)

## Paramètres

Valeur de configuration pour un noeud.
Couplé à un noeud unique et à sa durée de vie.

Commande `ros2 param`. Paramètre `use_sim_time` commun à tout les nodes.

## Launchfiles

Principe similaire à ROS1, utilisé pour lancer un environnement composé de plusieurs noeuds, remaps les topics, donner des paramètres...
Contrairement à ROS1, choix du langage parmi:

- Python => Possibilité de scripter les launchs, mais basiquement illisible
- XML => Très similaire aux launchfiles ROS1, quelques changements pour migration
- YAML => Why not

Commande `ros2 launch`

## Bags

`ros2 bag`, similaire à ROS1 dans l'utilisation

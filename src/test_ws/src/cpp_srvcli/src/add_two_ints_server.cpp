#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/srv/add_two_ints.hpp"

using example_interfaces::srv::AddTwoInts;

void add(const std::shared_ptr<AddTwoInts::Request> req, std::shared_ptr<AddTwoInts::Response> res)
{
    res->sum = req->a + req->b;

    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Incoming request: a: %ld, b: %ld", req->a, req->b);
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Sending back response: [%ld]", res->sum);
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    auto node = rclcpp::Node::make_shared("add_two_ints_server");
    auto service = node->create_service<example_interfaces::srv::AddTwoInts>("add_two_ints", &add);

    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Ready to add ints !");

    rclcpp::spin(node);
    rclcpp::shutdown();
}

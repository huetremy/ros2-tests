#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

using std_msgs::msg::String;

int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    auto node = rclcpp::Node::make_shared("republisher");

    auto pub = node->create_publisher<String>("pub_topic", 10);
    auto sub = node->create_subscription<String>("sub_topic", 10, [pub](const String& msg){
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Republishing message: %s", msg.data.c_str());
        pub->publish(msg);
    });

    rclcpp::spin(node);
    rclcpp::shutdown();
}

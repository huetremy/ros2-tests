import rclpy

from rclpy.node import Node
from rcl_interfaces.msg import ParameterDescriptor

class MinimalParam(Node):
    def __init__(self):
        super().__init__("param_node")
        self.timer = self.create_timer(2, self.timer_callback)

        my_parameter_descriptor = ParameterDescriptor(description="Test parameter")
        self.declare_parameter("my_parameter", "world", my_parameter_descriptor)

    def timer_callback(self):
        my_param = self.get_parameter("my_parameter")
        self.get_logger().info('Hello %s' % my_param)

def main():
    rclpy.init()
    node = MinimalParam()
    rclpy.spin(node)

    node.destroy_node()
    rclpy.shutdown()
    
if __name__ == "__main__":
    main()

#include <string>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "rcl_interfaces/msg/parameter_descriptor.hpp"

using namespace std::chrono_literals;

class ParametersClass: public rclcpp::Node
{
  public:
    ParametersClass()
      : Node ("parameter_node")
      {

        rcl_interfaces::msg::ParameterDescriptor my_param;
        my_param.description = "Test parameter";
        my_param.read_only = true;

        this->declare_parameter<std::string>("my_parameter", "world", my_param);
        timer_ = this->create_wall_timer(1s, [this](){
            this->respond();
        });
      }

    void respond()
    {
        this->get_parameter("my_parameter", parameter_string_);
        RCLCPP_INFO(this->get_logger(), "Hello %s", parameter_string_.c_str());
    }

  private:
    std::string parameter_string_;
    rclcpp::TimerBase::SharedPtr timer_;
};

int main(int argc, const char *const *argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<ParametersClass>());
    rclcpp::shutdown();

    return 0;
}

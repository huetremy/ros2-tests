#include <memory>
#include <thread>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

#include "action_tutorials_interfaces/action/fibonacci.hpp"

using action_tutorials_interfaces::action::Fibonacci;
using  GoalHandleFibonacci = rclcpp_action::ServerGoalHandle<Fibonacci>;

namespace action_tutorials_cpp
{
class FibonacciActionServer: public rclcpp::Node
{
  public:
    explicit FibonacciActionServer(const rclcpp::NodeOptions& options = rclcpp::NodeOptions())
    : Node("fibonacci_action_server", options)
    {
        this->action_server_ = rclcpp_action::create_server<Fibonacci>(
            this,
            "fibonacci",
            [this](const rclcpp_action::GoalUUID& uuid, std::shared_ptr<const Fibonacci::Goal> goal){
                return this->handle_goal(uuid, goal);
            },
            [this](const std::shared_ptr<GoalHandleFibonacci> goal_handle){
                return this->handle_cancel(goal_handle);
            },
            [this](const std::shared_ptr<GoalHandleFibonacci> goal_handle){
                return this->handle_accepted(goal_handle);
            }
        );
        RCLCPP_INFO(this->get_logger(), "Created action server");
    }

  private:
    rclcpp_action::Server<Fibonacci>::SharedPtr action_server_;

    rclcpp_action::GoalResponse handle_goal(const rclcpp_action::GoalUUID& uuid, std::shared_ptr<const Fibonacci::Goal> goal)
    {
        RCLCPP_INFO(this->get_logger(), "Received goal request with order %d", goal->order);
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse handle_cancel(const std::shared_ptr<GoalHandleFibonacci> goal_handle)
    {
        RCLCPP_INFO(this->get_logger(), "Received request to cancel goal");
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void handle_accepted(const std::shared_ptr<GoalHandleFibonacci> goal_handle)
    {
        std::thread{[this, goal_handle](){
            this->execute(goal_handle);
        }}.detach();
    }

    void execute(const std::shared_ptr<GoalHandleFibonacci> goal_handle)
    {
        RCLCPP_INFO(this->get_logger(), "Executing goal");
        rclcpp::Rate loop_rate{1};
        const auto goal = goal_handle->get_goal();
        auto feedback = std::make_shared<Fibonacci::Feedback>();
        auto& sequence = feedback->partial_sequence;
        sequence.push_back(0);
        sequence.push_back(1);
        auto result = std::make_shared<Fibonacci::Result>();

        for (int i = 1; (i < goal->order) && rclcpp::ok(); ++i) {
            // Check for cancel request
            if (goal_handle->is_canceling()) {
                result->sequence = sequence;
                goal_handle->canceled(result);
                RCLCPP_INFO(this->get_logger(), "Goal canceled");
                return;
            }

            // Update sequence
            sequence.push_back(sequence[i] + sequence[i - 1]);
            // Publish feedback
            goal_handle->publish_feedback(feedback);
            RCLCPP_INFO(this->get_logger(), "Publishing feedback");

            loop_rate.sleep();
        }

        if (rclcpp::ok()) {
            result->sequence = sequence;
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeded");
        }
    }
};
}

int main(int argc, const char *const *argv) 
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<action_tutorials_cpp::FibonacciActionServer>());
    rclcpp::shutdown();
}

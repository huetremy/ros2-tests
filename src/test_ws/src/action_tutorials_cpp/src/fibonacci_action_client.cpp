#include <future>
#include <memory>
#include <string>
#include <sstream>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

#include "action_tutorials_interfaces/action/fibonacci.hpp"

using namespace std::chrono_literals;

using action_tutorials_interfaces::action::Fibonacci;
using GoalHandleFibonacci = rclcpp_action::ClientGoalHandle<Fibonacci>;

namespace action_tutorials_cpp
{
class FibonacciActionClient: public rclcpp::Node
{
  public:
    FibonacciActionClient()
    : Node ("fibonacci_action_client")
    {
        this->client_ptr_ = rclcpp_action::create_client<Fibonacci>(this, "fibonacci");
        this->timer_ = this->create_wall_timer(500ms, [this](){
            return this->send_goal();
        });
    }

    void send_goal()
    {
        this->timer_->cancel();

        if (!this->client_ptr_->wait_for_action_server()) {
            RCLCPP_ERROR(this->get_logger(), "Action server not available");
            rclcpp::shutdown();
        }

        Fibonacci::Goal goal_msg{};
        goal_msg.order = 10;


        RCLCPP_INFO(this->get_logger(), "Sending goal");

        rclcpp_action::Client<Fibonacci>::SendGoalOptions goal_options {};
        goal_options.goal_response_callback = [this](const GoalHandleFibonacci::SharedPtr& goal_handle){
            return this->goal_response_callback(goal_handle);
        };
        goal_options.feedback_callback = [this](GoalHandleFibonacci::SharedPtr goal_handle, const std::shared_ptr<const Fibonacci::Feedback> feedback) {
            return this->feedback_callback(goal_handle, feedback);
        };
        goal_options.result_callback = [this](const GoalHandleFibonacci::WrappedResult& result){
            return this->result_callback(result);
        };

        this->client_ptr_->async_send_goal(goal_msg, goal_options);
    }

  private:
    rclcpp_action::Client<Fibonacci>::SharedPtr client_ptr_;
    rclcpp::TimerBase::SharedPtr timer_;

    void goal_response_callback(const GoalHandleFibonacci::SharedPtr& goal_handle)
    {
        if (!goal_handle) {
            RCLCPP_ERROR(this->get_logger(), "Goal was rejected by server");
        } else {
            RCLCPP_INFO(this->get_logger(), "Goal accepted by server, waiting for result");
        }
    }

    void feedback_callback(GoalHandleFibonacci::SharedPtr, const std::shared_ptr<const Fibonacci::Feedback> feedback)
    {
        std::stringstream ss;
        ss << "Next number in sequence received: ";
        for (const auto number: feedback->partial_sequence) {
            ss << number << " ";
        }
        RCLCPP_INFO(this->get_logger(), ss.str().c_str());
    }

    void result_callback(const GoalHandleFibonacci::WrappedResult& result)
    {
        switch (result.code) {
        case rclcpp_action::ResultCode::SUCCEEDED:
            break;
        case rclcpp_action::ResultCode::ABORTED:
            RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
            return;
        case rclcpp_action::ResultCode::CANCELED:
            RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
            return;
        default:
            RCLCPP_ERROR(this->get_logger(), "Unknown result code");
            return;
        }
        std::stringstream ss;
        ss << "Result received: ";
        for (const auto number: result.result->sequence) {
            ss << number << " ";
        }
        RCLCPP_INFO(this->get_logger(), ss.str().c_str());
        rclcpp::shutdown();
    }
};
}

int main(int argc, const char *const *argv)
{
    rclcpp::init(argc , argv);
    rclcpp::spin(std::make_shared<action_tutorials_cpp::FibonacciActionClient>());
    rclcpp::shutdown();
}

import rclpy
from rclpy.node import Node

from example_interfaces.srv import AddTwoInts

class MinimalService(Node):
    def __init__(self):
        super().__init__("minimal_service")
        self.srv = self.create_service(AddTwoInts, "add_two_ints", self.add_two_ints_callback)

    def add_two_ints_callback(self, request: AddTwoInts.Request, response: AddTwoInts.Response):
        response.sum = request.a + request.b
        self.get_logger().info('Incoming request: a: %d b: %d' % (request.a, request.b))

        return response

def main():
    rclpy.init()

    serv = MinimalService()
    rclpy.spin(serv)

    rclpy.shutdown()

if __name__ == '__main__':
    main()

// Copyright 2022 Heudiasyc UMR CNRS 7253
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "simple_components/subscriber_node.hpp"

SubscriberNode::SubscriberNode(rclcpp::NodeOptions options)
: Node("subscriber_node", options.use_intra_process_comms(true))
{
  sub_ = this->create_subscription<std_msgs::msg::Int32>(
    "int_topic", 20, [this](std_msgs::msg::Int32::UniquePtr msg) {
      RCLCPP_INFO(
        this->get_logger(), "Subscriber received: %d with address 0x%lX", msg->data,
        reinterpret_cast<uintptr_t>(msg.get()));
    });
}

#include "rclcpp_components/register_node_macro.hpp"

RCLCPP_COMPONENTS_REGISTER_NODE(SubscriberNode)

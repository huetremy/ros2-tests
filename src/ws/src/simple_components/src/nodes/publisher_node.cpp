// Copyright 2022 Heudiasyc UMR CNRS 7253
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "simple_components/publisher_node.hpp"

#include <chrono>

using namespace std::chrono_literals;

PublisherNode::PublisherNode(rclcpp::NodeOptions options)
: Node("publisher_node", options.use_intra_process_comms(true)), counter_{0}
{
  pub_ = this->create_publisher<std_msgs::msg::Int32>("int_topic", 20);
  timer_ = this->create_wall_timer(500ms, [this]() {
    auto msg = std::make_unique<std_msgs::msg::Int32>();
    msg->data = this->counter_++;
    RCLCPP_INFO(
      this->get_logger(), "Publisher : %d with address 0x%lX", msg->data,
      reinterpret_cast<std::uintptr_t>(msg.get()));
    this->pub_->publish(std::move(msg));
  });
}

#include "rclcpp_components/register_node_macro.hpp"

RCLCPP_COMPONENTS_REGISTER_NODE(PublisherNode)

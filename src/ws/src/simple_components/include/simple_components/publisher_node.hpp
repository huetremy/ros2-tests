// Copyright 2022 Heudiasyc UMR CNRS 7253
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SIMPLE_COMPONENTS__PUBLISHER_NODE_HPP_
#define SIMPLE_COMPONENTS__PUBLISHER_NODE_HPP_

#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "simple_components/visibility_control.h"
#include "std_msgs/msg/int32.hpp"

class PublisherNode : public rclcpp::Node
{
public:
  SIMPLE_COMPONENTS_CPP_PUBLIC PublisherNode(rclcpp::NodeOptions options);

private:
  std::size_t counter_;
  std::shared_ptr<rclcpp::Publisher<std_msgs::msg::Int32>> pub_;
  std::shared_ptr<rclcpp::TimerBase> timer_;
};

#endif  // SIMPLE_COMPONENTS__PUBLISHER_NODE_HPP_
